let content = document.querySelector('.data-content');
async function _getData() {
    try {
        const response = await fetch('https://ghibliapi.herokuapp.com/films');
        const data = await response.json();
        return data;
    } catch (error) {
        console.log(error);
    }
}

function _getDataPromise() {
    return fetch('https://ghibliapi.herokuapp.com/films');
}
_getDataPromise()
    .then(data => console.log(data.json))
    .catch(e => console.log('ERROR', e));


function _consumeGhibliAPI() {
    fetch('https://ghibliapi.herokuapp.com/films')
        .then(data => console.log(data.json()))
        .catch(error => console.log('Ups! Ocurrió un error', error));
}

