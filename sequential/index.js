const { taskOne, taskTwo } = require('./tasks');

async function main() {
    try {
        console.time('Measuring time');
        const firstValue = await taskOne();
        const secondValue = await taskTwo();
        console.timeEnd('Measuring time');
        console.log('Task one returned', firstValue);
        console.log('Task two returned', secondValue);
    } catch (error) {
        console.log(error);
    }
}
main();
